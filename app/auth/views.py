from flask import flash, redirect, render_template, url_for, session, request
from flask_login import login_required, login_user, logout_user

from . import auth
from forms import LoginForm, RegistrationFrom
from .. import db
from ..models import User, Activity, PhysicalInfo

import json

from ..accesslink.accesslink import AccessLink
from utils import load_config, save_config, pretty_print_json
import requests

CALLBACK_PORT = 5000
CALLBACK_ENDPOINT = "/oauth2_callback"

CONFIG_FILENAME = "config.yml"

REDIRECT_URL = "http://localhost:{}{}".format(CALLBACK_PORT, CALLBACK_ENDPOINT)

config = load_config(CONFIG_FILENAME)

accesslink = AccessLink(client_id=config['client_id'],
                        client_secret=config['client_secret'],
                        redirect_url=REDIRECT_URL)


@auth.route('/register', methods=['GET', 'POST'])
def register():
    """
    Handle requests to the /register route
    Add an employee to database through registration form
    """
    form = RegistrationFrom()
    if form.validate_on_submit():
        user = User(email=form.email.data,
                    username=form.username.data,
                    first_name=form.first_name.data,
                    last_name=form.last_name.data,
                    password=form.password.data)

        # add employee to the database
        db.session.add(user)
        db.session.commit()
        flash('You have successfully registered. You may now login.')

        # redirect to the login page
        return redirect(url_for('auth.login'))

    # load registration template
    return render_template('auth/register.html', form=form, title='Register')


@auth.route('/login', methods=['GET', 'POST'])
def login():
    """
    Handle requests to the /login route
    Log an employee in through the login form
    """
    form = LoginForm()
    if form.validate_on_submit():
        # check whether employee exists in the database and whether
        # the password entered matches the password in the database
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            # log employee in
            login_user(user)

            # session['username'] = employee.username
            config["username"] = user.username
            config["id"] = user.id
            save_config(config, CONFIG_FILENAME)

            if not user.is_admin and user.access_token is not None:
                """ Get user information """
                user_info = accesslink.users.get_information(user_id=user.user_id,
                                                             access_token=user.access_token)

                config["user_info"] = pretty_print_json(user_info)
                save_config(config, CONFIG_FILENAME)

                """ Get available data """
                check_available_data(id=user.id, user_id=user.user_id, access_token=user.access_token)

            # redirect to the appropriate dashboard page after login
            if user.is_admin:
                return redirect(url_for('home.admin_dashboard'))
            else:
                return redirect(url_for('home.dashboard'))
        else:
            flash('Error: Invalid email or password')

    # load login template
    return render_template('auth/login.html', form=form, title='Login')


@auth.route('/logout')
@login_required
def logout():
    """
    Handle requests to the /logout route
    Log an employee out through the logout link
    """
    session.pop('username', None)
    logout_user()
    # flash('You have successfully been logged out.')

    # redirect to the home page
    # return redirect(url_for('auth.login'))
    return redirect(url_for('home.homepage'))


@auth.route('/authorize', methods=['GET', 'POST'])
@login_required
def authorize():
    """
    Authenticate user with Polar service
    """
    return redirect(accesslink.authorization_url)


@auth.route(CALLBACK_ENDPOINT)
def callback():
    """Callback for OAuth2 authorization request

    Saves the user's id and access token to a file.
    """

    #
    # Get authorization from the callback request parameters
    #
    authorization_code = request.args.get("code")

    #
    # Get an access token for the user using the authorization code.
    #
    # The authorization code is only valid for 10 minutes, so the access token
    # should be fetched immediately after the authorization step.
    #
    token_response = accesslink.get_access_token(authorization_code)

    #
    # Save the user's id and access token to the configuration file.
    #
    # config["user_id"] = token_response["x_user_id"]
    # config["access_token"] = token_response["access_token"]
    # save_config(config, CONFIG_FILENAME)

    #
    # Save user's id and access token to the database
    #

    config = load_config(CONFIG_FILENAME)

    user_name = config["username"]
    user = User.query.filter_by(username=user_name).first()
    user.user_id = token_response["x_user_id"]
    user.access_token = token_response["access_token"]
    db.session.add(user)
    db.session.commit()

    #
    # Register the user as a user of the application.
    # This must be done before the user's data can be accessed through AccessLink.
    #
    try:
        # accesslink.users.register(access_token=config["access_token"])
        accesslink.users.register(access_token=user.access_token)
    except requests.exceptions.HTTPError as err:
        # Error 409 Conflict means that the user has already been registered for this client.
        # That error can be ignored in this example.
        if err.response.status_code != 409:
            raise err

    return redirect(url_for('home.dashboard'))


def check_available_data(id, user_id, access_token):
    available_data = accesslink.pull_notifications.list()

    if not available_data:
        print("No new data available.")
        return

    print("Available data:")
    print(pretty_print_json(available_data))

    for item in available_data["available-user-data"]:
        if item["data-type"] == "EXERCISE":
            get_exercises(user_id=user_id, access_token=access_token)
        elif item["data-type"] == "ACTIVITY_SUMMARY":
            get_daily_activity(id, user_id=user_id, access_token=access_token)
        elif item["data-type"] == "PHYSICAL_INFORMATION":
            get_physical_info(id, user_id=user_id, access_token=access_token)


def get_daily_activity(id, user_id, access_token):
    transaction = accesslink.daily_activity.create_transaction(user_id=user_id,
                                                               access_token=access_token)
    if not transaction:
        print("No new daily activity available.")
        return

    resource_urls = transaction.list_activities()["activity-log"]

    for url in resource_urls:
        activity_summary = transaction.get_activity_summary(url)

        print("Activity summary:")
        print(pretty_print_json(activity_summary))
        activity = Activity(user_id=id,
                            id=activity_summary["id"],
                            polar_user=activity_summary["polar-user"],
                            date=activity_summary["date"],
                            transaction_id=activity_summary["transaction-id"],
                            created=activity_summary["created"],
                            calories=activity_summary["calories"],
                            active_calories=activity_summary["active-calories"],
                            duration=activity_summary["duration"],
                            active_steps=activity_summary["active-steps"])
        db.session.add(activity)
        db.session.commit()

    transaction.commit()


def get_physical_info(id, user_id, access_token):
    transaction = accesslink.physical_info.create_transaction(user_id=user_id,
                                                              access_token=access_token)
    if not transaction:
        print("No new physical information available.")
        return

    resource_urls = transaction.list_physical_infos()["physical-informations"]

    for url in resource_urls:
        physical_info = transaction.get_physical_info(url)

        print("Physical info:")
        p_info = PhysicalInfo(
            user_id=id,
            id=physical_info["id"],
            transaction_id=physical_info["transaction-id"],
            created=physical_info["created"],
            polar_user=physical_info["polar-user"],
            weight=physical_info["weight"],
            height=physical_info["height"],
            # maximum_heart_rate=physical_info["maximum-heart-rate"],
            # resting_heart_rate=physical_info["resting-heart-rate"],
            # aerobic_threshold=physical_info["aerobic-threshold"],
            # anaerobic_threshold=physical_info["anaerobic-threshold"],
            # vo2_max=physical_info["vo2-max"],
            # weight_source=physical_info["weight-source"]
            )
        db.session.add(p_info)
        db.session.commit()

        print(pretty_print_json(physical_info))

    transaction.commit()


def get_exercises(user_id, access_token):
    transaction = accesslink.training_data.create_transaction(user_id=user_id,
                                                              access_token=access_token)
    if not transaction:
        print("No new exercises available.")
        return

    resource_urls = transaction.list_exercises()["exercises"]

    for url in resource_urls:
        exercise_summary = transaction.get_exercise_summary(url)

        print("Exercise summary:")

        print(pretty_print_json(exercise_summary))

    transaction.commit()