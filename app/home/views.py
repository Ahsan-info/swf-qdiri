from flask import render_template, abort, render_template, request
from flask_login import login_required, current_user
import json

from sqlalchemy.orm import session

from utils import pretty_print_json, load_config

CONFIG_FILENAME = "config.yml"

from . import home
from ..models import User, Activity, PhysicalInfo

from sqlalchemy.sql import func
from .. import db

labels = [
    'JAN', 'FEB', 'MAR', 'APR',
    'MAY', 'JUN', 'JUL', 'AUG',
    'SEP', 'OCT', 'NOV', 'DEC'
]

values = [
    967.67, 1190.89, 1079.75, 1349.19,
    2328.91, 2504.28, 2873.83, 4764.87,
    4349.29, 6458.30, 9907, 16297
]

colors = [
    "#F7464A", "#46BFBD", "#FDB45C", "#FEDCBA",
    "#ABCDEF", "#DDDDDD", "#ABCABC", "#4169E1",
    "#C71585", "#FF4500", "#FEDCBA", "#46BFBD"]


@home.route('/')
def homepage():
    """
    Render homepage template on teh / route
    """
    return render_template('home/index.html', title='Welcome')


@home.route('/dashboard')
@login_required
def dashboard():
    """
    Render the dashboard template on the /dashboard route
    """
    config = load_config(CONFIG_FILENAME)
    # print(pretty_print_json(json.loads(config["user_info"])))
    # user_info = json.loads(config["user_info"])
    id = config["id"]
    user = User.query.filter_by(id=id).first()
    physical_info = PhysicalInfo.query.filter_by(user_id=id)
    activities = Activity.query.filter_by(user_id=id).all()
    sum_of_calories = []
    sum_of_steps = []
    total_steps = 0
    total_calories = 0
    avg_steps = 0
    avg_calories = 0
    for act in activities:
        sum_of_calories.append(act.calories)
        sum_of_steps.append(act.active_steps)

    if len(sum_of_calories) == 0:
        total_calories = 0
    else:
        total_calories = sum(sum_of_calories)
        avg_calories = sum(sum_of_calories)/len(sum_of_calories)
    if len(sum_of_steps) == 0:
        total_steps = 0
    else:
        total_steps = sum(sum_of_steps)
        avg_steps = sum(sum_of_steps) / len(sum_of_steps)

    return render_template('home/dashboard.html', title='Dashboard', user=user, physical_info=physical_info,
                           total_steps=total_steps, avg_steps=avg_steps, total_calories=total_calories,
                           avg_calories=avg_calories, labels=labels, values=values)


@home.route('/admin/dashboard')
@login_required
def admin_dashboard():
    # prevent non-admins from accessing the page
    if not current_user.is_admin:
        abort(403)

    return render_template('home/admin_dashboard.html', title="Dashboard")
