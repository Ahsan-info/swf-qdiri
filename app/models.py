from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, login_manager


class User(UserMixin, db.Model):
    """
    Create an user table
    """
    __tablename__ = 'users'

    id= db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(60), index=True, unique=True)
    username = db.Column(db.String(60), index=True, unique=True)
    first_name = db.Column(db.String(60), index=True)
    last_name = db.Column(db.String(60), index=True)
    access_token = db.Column(db.String(60))
    user_id = db.Column(db.String(10), unique=True)
    registration_date = db.Column(db.String(60))
    member_id = db.Column(db.String(120))
    password_hash = db.Column(db.String(128))
    department_id = db.Column(db.Integer, db.ForeignKey('departments.id'))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    is_admin = db.Column(db.Boolean, default=False)
    activities = db.relationship('Activity', backref='user', lazy=True)
    physicalinfo = db.relationship('PhysicalInfo', backref='user', lazy=True)

    @property
    def password(self):
        """
        Prevent password from being accessed
        """
        raise AttributeError('password is nto a readable attribute')

    @password.setter
    def password(self, password):
        """
        Set password to a hashed password
        """
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """
        Check if hashed password matches actual password
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User: {}>'.format(self.username)

    # set up user_loader
    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(int(user_id))


class Department(db.Model):
    """
    Create a department table
    """

    __tablename__ = 'departments'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))
    users = db.relationship('User', backref='department',
                                lazy='dynamic')

    def __repr__(self):
        return '<Department: {}>'.format(self.name)


class Role(db.Model):
    """
    Create a Role table
    """

    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), unique=True)
    description = db.Column(db.String(200))
    users = db.relationship('User', backref='role',
                                lazy='dynamic')

    def __repr__(self):
        return '<Role: {}>'.format(self.name)


class Activity(db.Model):
    """"Create an activity table"""

    __tablename__ = 'activities'

    id = db.Column(db.Integer, primary_key=True)
    active_calories = db.Column(db.Integer)
    active_steps = db.Column(db.Integer)
    calories = db.Column(db.Integer)
    created = db.Column(db.String(200))
    date = db.Column(db.String(200))
    duration = db.Column(db.String(200))
    polar_user = db.Column(db.String(200))
    transaction_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __repr__(self):
        return '<Activity: {}>'.format(self.id)


class PhysicalInfo(db.Model):
    """ Create a physical info table """
    __tablename__ = 'physicalinfo'

    id = db.Column(db.Integer, primary_key=True)
    transaction_id = db.Column(db.Integer)
    created = db.Column(db.String(200))
    polar_user = db.Column(db.String(200))
    weight = db.Column(db.Float)
    height = db.Column(db.Float)
    maximum_heart_rate = db.Column(db.Integer)
    resting_heart_rate = db.Column(db.Integer)
    aerobic_threshold = db.Column(db.Integer)
    anaerobic_threshold = db.Column(db.Integer)
    vo2_max = db.Column(db.Integer)
    weight_source = db.Column(db.String(200))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)

    def __repr__(self):
        return '<PhysicalInfo: {}>'.format(self.id)


