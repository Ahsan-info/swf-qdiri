
class Config(object):
    """ Common configuration"""


class DevelopmentConfig(Config):
    """ Development configuation"""
    DEBUG = False
    SQLALCHEMY_ECHO = True


class ProductionConfig(Config):
    """ Production configuration"""
    DEBUG = False


app_config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}