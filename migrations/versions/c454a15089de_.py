"""empty message

Revision ID: c454a15089de
Revises: 
Create Date: 2018-04-10 19:32:39.688000

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c454a15089de'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('activities',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('active_calories', sa.Integer(), nullable=True),
    sa.Column('active_steps', sa.Integer(), nullable=True),
    sa.Column('calories', sa.Integer(), nullable=True),
    sa.Column('created', sa.String(length=200), nullable=True),
    sa.Column('date', sa.String(length=200), nullable=True),
    sa.Column('duration', sa.String(length=200), nullable=True),
    sa.Column('polar_user', sa.String(length=200), nullable=True),
    sa.Column('transaction_id', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('activities')
    # ### end Alembic commands ###
